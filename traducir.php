<?php 

function full_url ($url) {
	$patron1 = 'wiki';
	$patron2 = 'title';
	$pos[] = strpos($url, $patron1, 0);
	$pos[0] += 5;
	$pos[] = strpos($url, $patron2, $pos[0]);
	$pos[1] -= 2;
	$cacho = substr($url, $pos[0], $pos[1] - $pos[0] + 1);
	return $cacho;
}


function traduce ($url) {
	$url = str_replace("á", "&aacute;", $url);
	$url = str_replace("Á", "&Aacute;", $url);
	$url = str_replace("é", "&eacute;", $url);
	$url = str_replace("É", "&Eacute;", $url);
	$url = str_replace("í", "&iacute;", $url);
	$url = str_replace("Í", "&Iacute;", $url);
	$url = str_replace("ó", "&oacute;", $url);
	$url = str_replace("Ó", "&Oacute;", $url);
	$url = str_replace("ú", "&uacute;", $url);
	$url = str_replace("Ü", "&#220;", $url);
	$url = str_replace("ü", "&#250;", $url);
	$url = str_replace("Ú", "&Uacute;", $url);
	$url = str_replace("ñ", "&ntilde;", $url);
	$url = str_replace("Ñ", "&Ntilde;", $url);
	$url = str_replace("×", "&#215;", $url);
	$url = str_replace("ō", "&#333;", $url);
	$url = str_replace("+", "&#43;", $url);
	$url = str_replace("−", "&minus;", $url);
	$url = str_replace("·", "&#183;", $url);
	$url = str_replace("º", "&#186;", $url);
	$url = str_replace("—", "&#151;", $url);
	$url = str_replace("Æ", "&#198;", $url);
	$url = str_replace("æ", "&#230;", $url);
	$url = str_replace("a122284887525_", '', $url);
	$url = str_replace("//", ' ', $url);
	$url = str_replace('\r\n', '', $url);
	$url = str_replace('\r', '', $url);
	$url = str_replace('\n', '', $url);
	$url = str_replace('†', "&#134;", $url);
	$url = trim($url, '\t');
	$url = trim($url, '\n');
	$url = trim($url, '\r');
	$url = trim($url, '\0');
	$url = trim($url, '\x0B');
	$url = str_replace('"', '', $url);
	$url = quoted_printable_encode($url);
	$url = str_replace('=0D', '', $url);
	$url = str_replace('=0A', '', $url);
	$url = str_replace('= ', '', $url);
	$url = quoted_printable_decode($url);
	return $url;
}


function elimina_head ($texto) {
	$patron = 'fte.';
	$pos = strpos($texto, $patron);
	$pos += 27;
	if (!$pos) {
		$patron = 'src]';
		$pos = strpos($texto, $patron);
		$pos += 33;
	}
	if (!$pos) {
		$patron = 'class="r1';
		$pos = strpos($texto, $patron);
		$pos += 58;
	}

	$texto = substr($texto, $pos);
	return $texto;
}


function elimina_fin ($texto) {
	$patron = '<h2>Contenido';
	$pos[] = strpos($texto, $patron);
	$texto = substr($texto, 0, $pos[0] - 76);
	return $texto;
}


function elimina_enlaces ($texto) {
	$patron = '/<\w[^<]*>/';
	$texto = preg_replace($patron, '', $texto);
	$patron = '/<\/\w>/';
	$texto = preg_replace($patron, '', $texto);
	return $texto;
}


function elimina_tablas ($texto) {
	$patron1 = '<table';
	$patron2 = 'table>';

	$desfase = 0;
	$i = 1;
	$ini[] = strpos($texto, $patron1, $desfase);
	$ini[] = strpos($texto, $patron1, $desfase);
	$fin = strpos($texto, $patron2, $desfase);
	while ($fin > 0) {	
		$continuar = true;
		while ($ini[$i] < $fin && $ini[$i] > 0 && $continuar) {
			$desfase = $ini[$i] + 1;
			$ini[] = strpos($texto, $patron1, $desfase);
			$i++;
			if (!($ini[$i] > 0)) {
				$continuar = false;
			}
		}
		if (($ini[$i] > $fin && $ini[$i - 1] < $fin) || (!$continuar)) {
			$cacho1 = substr($texto, 0, $ini[$i - 1]);
			$cacho2 = substr($texto, $fin + 6);
			unset($texto);
			$texto = $cacho1 . $cacho2;
			$desfase = 0;
			$i = 1;
			unset($ini);
			unset($fin);
			$ini[] = strpos($texto, $patron1, $desfase);
			$ini[] = strpos($texto, $patron1, $desfase);
			$fin = strpos($texto, $patron2, $desfase);
		}
	}
	return $texto;
}


function inicio_destacado ($texto) {
	$patron = '<ul';
	$pos[] = strpos($texto, $patron);
	$texto = substr($texto, $pos[0] + 31);
	return $texto;
}


function fin_destacado ($texto) {
	$patron = '</ul>(';
	$pos[] = strpos($texto, $patron);
	$texto = substr($texto, 0, $pos[0]);
	return $texto;
}


?>

